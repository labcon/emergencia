import React from 'react'
import Layout from '@/components/Layout'
import Seo from '@/components/Seo'
import Posts from '@/components/Posts'
import StartHero from '@/components/StartHero'
import Intro from '@/components/Intro'

class IndexPage extends React.Component {
  render() {

    return (
      <Layout>
        <Seo />
        <StartHero />
        <Intro postid="3858" />
        <Posts />
      </Layout>
    )
  }
}

export default IndexPage
