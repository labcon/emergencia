// import React from 'react';
// import { StaticQuery, graphql } from 'gatsby';
// import Img from 'gatsby-image';
// import BgImage from './BgImage';
// import { StNonProcessedImage } from './style.css';

// const Image = (props) => (
//   <StaticQuery
//     query={graphql`
// 			query{
// 				images: allFile{
// 					edges {
// 						node {
// 							relativePath
// 							name
// 							publicURL
// 							childImageSharp {
// 								fullwidth: fluid(
// 									maxWidth: 1200,
// 									quality: 80
// 								) {
// 									...GatsbyImageSharpFluid
// 								},
// 							},
// 						}
// 					}
// 				}
// 			}
// 		`}
//     render={(data) => {
//       const image = data.images.edges.find((n) => {
//         if (props.filename) {
//           console.log("filename!", props.filename)
//           return n.node.relativePath.includes(props.filename)
//         }
//         return null
//       })

//       if (!image) {
//         return null
//       }

//       const imageSizes = (imgtype) => {
//         switch (imgtype) {
//           default:
//             return image.node.childImageSharp.fullwidth
//         }
//       }

//       if (props.bg) {
//         return <BgImage alt={props.alt} sizes={imageSizes(props.type)} />;
//       }

//       return <Img loading="eager" alt={props.alt} sizes={imageSizes(props.type)} objectFit="contain" objectPosition="50% 50%" />
//     }}
//   />
// );

// export default Image;
