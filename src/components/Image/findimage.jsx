import { useStaticQuery, graphql } from 'gatsby'

const FindImage = (originalurl, filename, format) => {

  const imagequery = useStaticQuery(
    graphql`
			query {
				imagesinacf: allContentImages(filter: {contentImageSource: {eq: "attached_images_in_acf"}}) {
					edges {
						node {
							url
              contentImageSource
              imageFile{
								childImageSharp {
									image: fixed(
										width: 100,
										height: 100,
										pngQuality: 0,
										duotone: {
											highlight: "#ff5f55",
											shadow: "#000000"
										},
										toFormat: PNG,
										traceSVG: {
											color: "#330606"
										}
									) {
										...GatsbyImageSharpFixed_tracedSVG,
									}
                }
              }
						}
					}
				}
        contentimages: allContentImages(filter: {contentImageSource: {eq: "attached_contentimages"}}) {
					edges {
						node {
							url
              contentImageSource
              imageFile{
								childImageSharp {
									image: fluid(
										maxWidth: 700,
										quality: 50,
										srcSetBreakpoints: [ 340, 700 ],
									) {
										...GatsbyImageSharpFluid
									},
                }
              }
						}
					}
				}
        mainimg: allContentImages(filter: {contentImageSource: {eq: "attached_mainimg"}}) {
					edges {
						node {
							url
              contentImageSource
              imageFile{
								childImageSharp {
									image: fixed(
										width: 1200,
                    height: 630,
										quality: 50
									) {
										...GatsbyImageSharpFixed
									}
                }
              }
						}
					}
				}
			}
    `
  )

  const imgarray = [
    ...imagequery.imagesinacf.edges,
    ...imagequery.contentimages.edges,
    ...imagequery.mainimg.edges
  ]

  const queriedpost = imgarray.find((n) => {
    if(format && originalurl) {
      return n.node.url === originalurl && n.node.contentImageSource === format
    } else
    if (format && filename) {
      return n.node.url.includes(filename) && n.node.contentImageSource === format
    } else
    if (originalurl) {
      return n.node.url === originalurl
    } else
    if(filename) {
      return n.node.url.includes(filename)
    }
  })
  if (!queriedpost) {
    return null
  }

  return queriedpost
  // return null
}

export default FindImage
