import styled from 'styled-components';
import theme from '../../../styles/theme';

export const StNav = styled.nav`
  position: relative;
  top: 28px;
  a {
    font-family: 'Acta Sans', sans-serif;
    font-weight: bold;
    width: 100%;
    font-size: 0.7rem;
    text-transform: uppercase;
    text-align: center;
    color: ${theme.color};
    margin-right: 0.5rem;
    padding: 0.2rem;
    transition: all .3s;
    &:hover{
      background-color: ${theme.color};
      color: ${theme.background};
    }
  }
`;