import React from 'react'
import { Link } from 'gatsby'
import { StNav } from './style.css'
import QueryGetByTag from '../../../queries/QueryGetByTag'

const Nav = ({ active, handleToggleMenu }) => {

	const Posts = QueryGetByTag(['video-emergencia'])

	const MenuItem = ({post}) => {
		const {slug, title, acf} = post
		const { titulo_resumido = title } = acf
		return (
			<Link to={`/${slug}/`}>{titulo_resumido}</Link>
		)
	}

	const ArrPosts = () => (
		<>
			{
				Posts.map((post, index) => {
					return (
						<MenuItem post={post} key={index} />
					)
				})
			}
		</>
	)

	return (
		<StNav>
			<ArrPosts />
		</StNav>
	)
}

export default Nav