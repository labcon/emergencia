import styled from 'styled-components';
import theme from '@/styles/theme';

const Container = styled.header`
	display: flex;
	justify-content: space-between;
	align-items: center;
	position: fixed;
	width: 100%;
	padding: 1rem 1.3rem;
	z-index: 4;
	top: 0;
	background-color: transparent;
	height: ${theme.headerHeight};
	/* overflow: hidden; */
	.menu-button{
		color: ${theme.clearcolor};
	}
	&:after{
		content: '';
		position: absolute;
		background: linear-gradient(rgba(0,0,0,0.6), transparent);
		left: 0;
		right: 0;
		top: 0;
		height: 10vh;
		z-index: -1;
	}
	.col-menu{
		@media (max-width: 1250px){
			display: none;
		}
	}
	.col{
		display: flex;
		align-items: center;
	}
	.btn-suscribete{
		background-color: ${theme.color};
		color: ${theme.background};
    text-decoration: none;
		border-radius: 6px;
		font-family: 'Open Sans', sans-serif;
		font-size: 0.8rem;
		padding: 0.4rem 0.6rem;
		text-transform: uppercase;
		margin: 6px 1rem;
		line-height: 0.8rem;
		display: block;
		border: 1px solid ${theme.color};
		&:hover{
			background-color: ${theme.background};
			color: ${theme.color};
			border: 1px solid ${theme.color};
		}
	}
	.col2{
		@media(max-width: 800px){
			display: none;
		}
	}


	.slotSponsor {
		font-size: 9px;

		color: white;
		span {
			display: block;
			padding-bottom: 2px;
		}
		svg,
		img {
			padding-left: 5px;
			min-width: 90px;
			@media (max-width: 800px) {
				min-width: 50px;
			}
		}
	}
	.slotLab {
		@media (max-width: 600px) {
			display: none;
		}
	}

	.slotLT {
		@media (max-width: 880px) {
			display: none;
		}
	}
`;

const StLogo = styled.h1`
	font-family: ${theme.fontbody};
	font-size: 9px;
	text-align: center;
	color: black;
	svg,
	img {
		min-width: 120px;
		max-width: 120px;
		max-height: 40px;
		@media (max-width: 800px) {
			max-height: 40px;
			min-width: 120px;
		}
	}
`;

export { Container, StLogo };
