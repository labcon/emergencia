import React, { useState } from 'react'
import { Link } from 'gatsby'
import { Container, StLogo } from './style.css'
import { LaterceraWhite } from '@labcon/copesa-logos'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import MobileNav from './MobileNav'
import QueryGetByTag from '../../queries/QueryGetByTag'

const Header = ({ sponsorLogo }) => {

	const posts = QueryGetByTag(['video-emergencia'])

	const [menu, updateMenu] = useState(false)

	return (
		<>
			<Container>
				<div className="col">

				</div>
				<div className="col">
					<Link to="/" className="slotSite">
						<StLogo>
							<LaterceraWhite />
						</StLogo>
					</Link>
				</div>
				<div className="col">
					<div className="menu-button" onClick={() => updateMenu(!menu)}>
						{
							menu ? (
								<FontAwesomeIcon icon={['fas', 'times']} />
							)	: (
								<FontAwesomeIcon icon={['fas', 'bars']} />
							)
						}
					</div>
				</div>
			</Container>
			<MobileNav posts={posts} active={menu} handleToggleMenu={(val) => updateMenu(val)}  />
		</>
	)
}

export default Header
