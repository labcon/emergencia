import React, {useState, useEffect} from 'react'
import {StProgressBar, StProgressBarWrap} from './style.css'
import _ from 'lodash'

const ProgressBar = ({color, height, adjustment = 0}) => {
  const [width, setWidth] = useState(0)

  const watchScrolling = () => {
    const { scrollHeight, clientHeight, scrollTop } = document.documentElement
    const winScroll = document.body.scrollTop || scrollTop
    const winHeight = scrollHeight - clientHeight
    const scrolled = `${(winScroll / (winHeight - adjustment )) * 100}%`
    if(winHeight > 0) {
      return setWidth(scrolled)
    } else {
      return setWidth(0)
    }
  }

  useEffect(
    () => {
      const watchThrottled = _.throttle(watchScrolling, 200)
      window.addEventListener('scroll', watchThrottled)
      return() => {
        window.removeEventListener('scroll', watchThrottled)
      }
    },
    [color, height, adjustment]
  )

  const styles = {
    progress: {
      width: width
    }
  }

  return (
    <StProgressBarWrap>
      <StProgressBar style={styles.progress} height={height} color={color} />
    </StProgressBarWrap>
  )
}

export default ProgressBar