import React from 'react'
import SuperStoryCard from '@/components/SuperStoryCard'
import QueryGetByTag from '@/queries/QueryGetByTag'

const Posts = ({actualSlug}) => {
  const posts = QueryGetByTag(['video-emergencia'])
  const postsCopy = [...posts]
  const actualIndex = actualSlug ? posts.findIndex(item => item.slug === actualSlug) : null
  const orderedPosts = actualIndex ? [...postsCopy.splice(actualIndex), ...postsCopy] : postsCopy

  return(
    <>
      {
        orderedPosts.map((post, index) => {
          if (actualSlug === post.slug){
            return null
          }
          return (
            <SuperStoryCard post={post} key={index} />
          )
        })
      }
    </>
  )
}


export default Posts