import React from 'react'

import {
	StCredits
} from './style.css'

const Credits = () => (
  <StCredits>
    <div className="creditsWrapper">
      <h2>Créditos</h2>

      <p>
        <div className="col">
          <span>Coordinación y edición:</span>
        </div>
        <div className="col">
          <strong>Tania Opazo</strong>
        </div>
      </p>

      <p>
        <div className="col">
          <span>Investigación y textos:</span>
        </div>
        <div className="col">
          <strong>Alison Vivanco</strong>
          <strong>Alejandra Jara</strong>
          <strong>Tania Opazo</strong>
          <strong>Daniela Cruzat</strong>
          <strong>Mirko Curihual</strong>
          <strong>Axel Christiansen</strong>
          <strong>Carlos Pérez</strong>
          <strong>Sebastián Carrizo</strong>
        </div>
      </p>

      <p>
        <div className="col">
          <span>Diseño:</span>
        </div>
        <div className="col">
          <strong>Álex Acuña Viera</strong> <strong>Alfredo Duarte</strong>
        </div>
      </p>

      <p>
        <div className="col">
          <span>Desarrollo Web:</span>
        </div>
        <div className="col">
          <strong>Álex Acuña Viera</strong>
        </div>
      </p>
    </div>

  </StCredits>
)

export default Credits

