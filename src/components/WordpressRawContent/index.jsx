import React from 'react'
import { parse } from '@wordpress/block-serialization-default-parser'
import WPComponent from './wpcomponents'
import { StWordpressRaw } from './style.css'

const WordpressRawContent = ( {raw, slug, title} ) => {
  const parsedContent = parse(raw)

  return (
    <StWordpressRaw>
      {
        parsedContent.map((block, index) => {
          return (
            <WPComponent key={index} block={block} slug={slug} title={title} />
          )
        })
      }
    </StWordpressRaw>
  )
}

export default WordpressRawContent