import React from 'react'
import { StACFHardcoded } from './style.css'
import Image from '../../../Image'

const ACFHardcoded = ({ block }) => {

  const { attrs: { data: { hardcodedid } = {} } = {} } = block

  if(!hardcodedid) {
    return null
  }

  return (
    <StACFHardcoded className = {hardcodedid}>
      {
        hardcodedid === 'links-laboratorio' && (
          <>
            <div className="links-cols">
              <div className="link">
                <a href="https://interactivo.latercera.com/entel-brechadigital/">
                  <figure>
                    <figcaption>
                      <Image filename="lab-brecha-digital.jpg" />
                      <h3>Brecha digital - Especial para Entel</h3></figcaption>
                  </figure>
                </a>
              </div>
              <div className="link">
                <a href="https://interactivo.latercera.com/amor-propio/">
                  <figure>
                    <figcaption>
                      <Image filename="lab-amor-propio.jpg" />
                      <h3>Amor Propio - Especial para Dove</h3></figcaption>
                  </figure>
                </a>
              </div>
              <div className="link">
                <a href="https://interactivo.latercera.com/transicion/">
                  <figure>
                    <figcaption>
                      <Image filename="lab-transicion.jpg" />

                      <h3>Transición - Especial editorial auspiciado por WOM.</h3>
                    </figcaption>
                  </figure>
                </a>
              </div>
            </div>
          </>
        )
      }
      {
        hardcodedid === 'links-culto' && (
          <>
            <div className="links-cols">

              <div className="link">
                <a href="http://culto.latercera.com/2019/01/04/fantastico-mundo-murakami/">
                  <figure>
                    <figcaption>
                      <Image filename="culto-murakami.jpg" />

                      <h3>Video: Conversaciones de culto, Haruki Murakami</h3>
                    </figcaption>
                  </figure>
                </a>
              </div>

              <div className="link">
                <a href="https://www.latercera.com/especiales-lt/nicanor-parra-la-revolucion-no-termina/">
                  <figure>
                    <figcaption>
                      <Image filename="culto-nicanor.jpg" />

                      <h3>Nicanor Parra, la revolución no termina</h3>
                    </figcaption>
                  </figure>
                </a>
              </div>

              <div className="link">
                <a href="https://www.latercera.com/especiales-lt/el-legado-de-bowie/">
                  <figure>
                    <figcaption>
                      <Image filename="culto-bowie.jpg" />

                      <h3>El legado de Bowie</h3>
                    </figcaption>
                  </figure>
                </a>
              </div>
            </div>
          </>
        )
      }
      {
        hardcodedid === 'links-paula' && (
          <>
            <div className="links-cols">

              <div className="link">
                <a href="http://www.paula.cl/reportajes-y-entrevistas/gato-sano-pena/">
                  <figure>
                    <figcaption>
                      <Image filename="paula-gato.jpg" />

                      <h3>Hablemos de amor: Cómo un gato sanó mi pena</h3>
                    </figcaption>
                  </figure>
                </a>
              </div>

              <div className="link">
                <a href="http://www.paula.cl/reportajes-y-entrevistas/mi-hijo-es-autista/">
                  <figure>
                    <figcaption>
                      <Image filename="paula-autista.jpg" />

                      <h3>Hablemos de Maternidad: Mi hijo es autista</h3>
                    </figcaption>
                  </figure>
                </a>
              </div>

              <div className="link">
                <a href="http://www.paula.cl/especial-inclusion/">
                  <figure>
                    <figcaption>
                      <Image filename="paula-inclusion.jpg" />

                      <h3>Especial Inclusión</h3>
                    </figcaption>
                  </figure>
                </a>
              </div>
            </div>
          </>
        )
      }
      {
        hardcodedid === 'links-quepasa' && (
          <>
            <div className="links-cols">
              <div className="link">
                <a href="https://www.latercera.com/que-pasa/noticia/increible-hallazgo-potro-congelado-42-mil-anos/625737/">
                  <figure>
                    <figcaption>
                      <Image filename="quepasa-caballo.jpg" />

                      <h3>Video: El increíble hallazgo de un potro congelado de hace 42 mil años</h3>
                    </figcaption>
                  </figure>
                </a>
              </div>
              <div className="link">
                <a href="https://www.latercera.com/la-tercera-tv/noticia/este-campo-futbol-esta-hecho-vasos-reciclados/601071/">
                  <figure>
                    <figcaption>
                      <Image filename="quepasa-plastico.jpg" />

                      <h3>Video: Este es el campo de fútbol que está hecho con vasos reciclados</h3>
                    </figcaption>
                  </figure>
                </a>
              </div>
              <div className="link">

                <a href="https://interactivo.latercera.com/eclipse/">
                  <figure>
                    <figcaption>
                      <Image filename="quepasa-eclipse.jpg" />

                      <h3>Especial Eclipse</h3>
                    </figcaption>
                  </figure>
                </a>
              </div>
            </div>
          </>
        )
      }

    </StACFHardcoded>
  )
}

export default ACFHardcoded