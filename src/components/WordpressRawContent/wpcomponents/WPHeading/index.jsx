import React from 'react'

const WPHeading = ({ block }) => (
  <div className="coreHeading" dangerouslySetInnerHTML={{ __html: block.innerHTML }} />
)

export default WPHeading