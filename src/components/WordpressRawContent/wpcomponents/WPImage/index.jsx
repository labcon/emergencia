import React from 'react'
import ReactHtmlParser from 'react-html-parser'
import Image from '@/components/Image'
import get from 'lodash/get'
import { StWPFigure } from './style.css'

const WPImage = ({block}) => {
  const parsedhtml = ReactHtmlParser(block.innerHTML)
  const classNames = get(block, 'attrs.className', '')

  if (parsedhtml[0].type === 'figure') {
    const figure = parsedhtml[0]
    const figcaption = figure.props.children.filter(element => element.type === 'figcaption')

    return (
      <StWPFigure className={classNames}>

        <Image originalurl={figure.props.children[0].props.src} />
        {
          figcaption.length > 0 && (
            <div className="figwrap">{figcaption}</div>
          )
        }
      </StWPFigure>
    )
  }
}

export default WPImage