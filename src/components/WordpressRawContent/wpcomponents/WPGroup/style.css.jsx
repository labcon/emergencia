import styled from 'styled-components'
import theme from '@/styles/theme'

const StWPGroup = styled.div`
  position: relative;
  &.lt-special-inverted{
    background-color: black;
    color: white;
    padding-top: ${theme.verticalSpacing};
    .wpcolumns{
      max-width: ${theme.maxWidth};
      margin-left: auto;
      margin-right: auto;
      &.logocolumn{
        grid-template-columns: 180px 1fr;
        align-items: center;
        @media(max-width: 800px){
          grid-template-columns: 1fr;
        }
        p{
          max-width: 100%;
          margin: 0;
        }
        .coreColumn{
          &:first-of-type{

            svg{
              max-height: 70px;
              max-width: 180px;
              @media(max-width: 800px){
                display: block;
                margin-left: auto;
                margin-right: auto;
                margin-bottom: ${theme.spacing};
                margin-top: ${theme.spacing};
              }
            }
          }
        }
      }
    }
  }
`

export {StWPGroup}

