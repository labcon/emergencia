import React from 'react'
import WPComponent from '../index'
import { StWPGroup } from './style.css'

const WPGroup = ({ block }) => {
  const { innerBlocks, attrs } = block
  const blockclasses = attrs.className || ''
  return (
    <StWPGroup className={blockclasses}>
      {
        innerBlocks.map((innerBlock, index) => {
          return (
            <WPComponent key={index} block={innerBlock} />
          )
        })
      }
    </StWPGroup>
  )
}

export default WPGroup