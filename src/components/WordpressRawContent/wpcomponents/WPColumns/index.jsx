import React from 'react'
import WPComponent from '../index'
import { StWPColumns } from './style.css'

const WPColumn = ({block}) => {
  const { innerBlocks } = block

  return (
    <div className="coreColumn">
      {
        innerBlocks.map((innerBlock, index) => {
          return (
            <WPComponent key={index} block={innerBlock} />
          )
        })
      }
    </div>
  )
}

const WPColumns = ({ block }) => {
  const {innerBlocks, attrs} = block
  const blockclasses = attrs.className || ''
  return (
    <StWPColumns cols={innerBlocks.length} className={`${blockclasses} wpcolumns`}>
      {
        innerBlocks.map((innerBlock, index) => {
          return (
            <WPColumn key={index} block={innerBlock} />
          )
        })
      }
    </StWPColumns>
  )
}

export default WPColumns