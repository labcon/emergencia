import styled from 'styled-components'

const StWPColumns = styled.div`
  display: grid;
  grid-template-columns: ${props => `repeat(${props.cols}, 1fr)` || 'auto'};
`

export { StWPColumns };