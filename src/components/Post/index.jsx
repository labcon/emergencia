import React from "react"
import { graphql } from "gatsby"
import { StyledInfo } from "../Content/style.css"
import Content from "../Content"

const Post = ({ post, single }) => {
  return (
    <StyledInfo>
      <div className="col">
        <div className="wrap">
          <Content {...post} single={single} ></Content>
        </div>
      </div>
    </StyledInfo>
  )
}

export default Post

export const postFragment = graphql`
  fragment PostContent on wordpress__POST {
      title
      slug
      raw
      excerpt
      attached_mainimg
      acf{
        autor
        frases {
          frase
        }
        imagenes_inicio
      }
  }
`