import styled from 'styled-components'
import theme from '../../styles/theme'
import { Link } from 'gatsby'
import Image from "gatsby-image"

const StyledGuide = styled.section`
  display: grid;
  grid-template-columns: repeat(4, 25%);
  overflow: hidden;
  @media (max-width: 800px){
    grid-template-columns: 1fr;
  }
`

const StyledVideos = styled.section`

`

const StyledLink = styled(Link)`
  text-decoration: none;
  color: inherit;
  position: relative;
  h2{
    margin: 0;
    &:after{
      content: '';
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
      height: 40%;
      background: linear-gradient(transparent, ${theme.background});
      z-index: -1;
    }
  }
`

const StyledSection = styled.section`
  text-decoration: none;
  background-color: ${props => props.bgColor || theme.color};
  display: block;
  .wrap{
    padding: 6rem 2rem;
    @media(max-width: 800px){
      padding: 3rem 1rem;
    }
  }
  .wrapbajada{
    text-align: center;
    line-height: 1.5;
    .content{
      p {
        font-size: 1.6rem;
        font-style: italic;
      }
    }
  }
  .title{
    font-size: 6rem;
    line-height: 6rem;
    font-weight: normal;
    margin: 2rem 0;
    flex: 0;
    color: ${props => props.titleColor || theme.background};
    max-width: ${theme.overWidth};
    margin-left: auto;
    margin-right: auto;
    @media(max-width: 1200px){
      font-size: 5rem;
      line-height: 5rem;
      margin: 1rem 0;
    }
    @media(max-width: 800px){
      font-size: 4rem;
      line-height: 4rem;
      margin: 0;
    }
  }

  .content {
    max-width: ${theme.overWidth};
    margin-left: auto;
    margin-right: auto;
    font-size: 1.3rem;
    line-height: 1.42857;
    color: ${props => props.color || theme.color};
  }

  .explain{
    display: flex;
    align-items: center;
    justify-content: center;
    color: ${props => props.color || theme.color};
    img{
      min-width: 40px;
    }
  }
`

const StyledPosts = styled.section`
  padding: 2rem 0;

`

const StyledPostBlockTitle = styled.div`
  background-color: ${theme.color};
  color: ${theme.background};
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 1rem;
  text-align: center;
  width: 25vw;
  height: 25vw;
  h1{
    font-size: 3rem;
    @media (max-width: 1200px){
      font-size: 2rem;
    }
    @media (max-width: 800px){
      font-size: 4rem;
    }

  }
  p{
    font-size: 1.2rem;
    @media (max-width: 1200px){
      font-size: 1rem;
    }
    @media (max-width: 800px){
      font-size: 1.2rem;
    }
  }
  @media(max-width: 800px){
    width: 100vw;
    height: 100vw;
    padding: 2rem;
  }
`

export { StyledLink, StyledPosts, StyledSection, StyledPostBlockTitle, StyledGuide, StyledVideos }