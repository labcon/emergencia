import React from 'react'
import { Link } from 'gatsby'
import Image from '../Image'
import { Container, StContent, StFigureImage, StSubpost, StBackButton} from './style.css'
import WordpressRawContent from '../WordpressRawContent'
import VideoPlayer from '../VideoPlayer'
import Share from '../Sharer'
import GetByWPID from '@/queries/GetByWPID'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Related from '@/components/Related'

const striphtml = (str) => (str.replace('&#8220;', '').replace('&#8221;', ''))

const Overall = ({ post, children, asocExpandida}) => {
  return (
    asocExpandida ? (
      <div className="overallLink">
        {children}
      </div>
    ) : (
      <Link className="overallLink" to={`/${post.slug}/`}>
        {children}
      </Link>
    )
  )
}

const Content = ({ title, image, excerpt, raw, slug, acf, attached_mainimg, single, asociacion_expandida}) => {

const autor = acf.autor ? acf.autor : null
const video = acf.video ? acf.video : null
const video_iframe = acf.video_iframe ? acf.video_iframe : null
const asociaciones = acf.asociaciones ? acf.asociaciones : null
const mostrarFoto = acf.mostrar_foto ? acf.mostrar_foto : null
const asocExpandida = acf.asociacion_expandida ? acf.asociacion_expandida : false

const LinkClick = ({ children }) => {
  return (
    <>
    {
      single ? (
        <article className="blockLinkClick">
          {children}
        </article>
      ) : (
          <Link className="blockLinkClick" to={`/${slug}/`}>
            {children}
          </Link>
        )
        }
    </>
  )
}

return (
  <Container>
    <StContent>
      <LinkClick to={slug} single={single}>
      {
        mostrarFoto && attached_mainimg && (
          <div className="header-margin">
            <Image originalurl={attached_mainimg} />
          </div>
        )
      }

      {!single && (

        <LinkClick to={slug} single={single}>
        <h1 className="title" dangerouslySetInnerHTML={{ __html: title }} />
        <div className="excerpt" dangerouslySetInnerHTML={{ __html: excerpt }} />
        {
          autor && (
            <div className="author">Por: <strong>{autor}</strong></div>
          )
        }
        </LinkClick>
      )}
      {
        !video && (
          <div className="share-wrap">
            <Share
              socialConfig={{
                twitterHandle: 'latercera',
                config: {
                  url: `https://interactivo.latercera.com/los-videos-del-estallido-social/${slug}/`,
                  title: `${striphtml(title)} | Los Videos del Estallido Social - Interactivos La Tercera`,
                  hashtags: ['18D']
                }
              }}
            />
          </div>
        )
      }
      {
        video_iframe && (
          <>
            <div className="postVideoPlayer">
              <VideoPlayer iframe={video_iframe} />
            </div>
            <div className="share-wrap">
              <Share
                socialConfig={{
                  twitterHandle: 'latercera',
                  config: {
                    url: `https://interactivo.latercera.com/los-videos-del-estallido-social/${slug}/`,
                    title: `${striphtml(title)} | Los Videos del Estallido Social - Interactivos La Tercera`,
                    hashtags: ['18D']
                  }
                }}
              />
            </div>
          </>
        )
      }
      {
        video && !video_iframe &&(
          <>
            <div className="postVideoPlayer">
              <VideoPlayer url={video} autoplay={false} controls={true} muted={false} />
            </div>
            <div className="share-wrap">
              <Share
                socialConfig={{
                  twitterHandle: 'latercera',
                  config: {
                    url: `https://interactivo.latercera.com/los-videos-del-estallido-social/${slug}/`,
                    title: `${title} | Los Videos del Estallido Social - Interactivos La Tercera`,
                    hashtags: ['18D']
                  }
                }}
              />
            </div>
          </>
        )
      }
      { single && (
        <WordpressRawContent raw={raw} slug={slug} title={title} />
        )
      }

      </LinkClick>
      {
        !single && (
          <span className="button-more">
            <FontAwesomeIcon className="icon" icon={['far', 'eye']} />
            ver más
          </span>
        )
      }
      {
        asociaciones && (
          <>
            {
              asociaciones.map((wpid, index) => {
                const subpost = GetByWPID(wpid)

                return (
                  <StSubpost key={index}>
                    <Overall post={subpost} asocExpandida={asocExpandida}>
                      {
                        subpost.acf.mostrar_foto && subpost.attached_mainimg && (
                          <StFigureImage>
                            <Image originalurl={subpost.attached_mainimg} alt={subpost.title} />
                          </StFigureImage>
                        )
                      }
                      <h1 className="title" dangerouslySetInnerHTML={{ __html: title }} />
                      <div className="excerpt" dangerouslySetInnerHTML={{ __html: subpost.excerpt }} />
                      {
                        subpost.acf.autor && (
                          <div className="author">Por: <strong>{subpost.acf.autor}</strong></div>
                        )
                      }
                      {
                        !subpost.acf.video && (
                          <div className="share-wrap">
                            <Share
                              socialConfig={{
                                twitterHandle: 'latercera',
                                config: {
                                  url: `https://interactivo.latercera.com/los-videos-del-estallido-social/${subpost.slug}/`,
                                  title: `${subpost.title} - Interactivos La Tercera`,
                                  hashtags: ['18D']
                                }
                              }}
                            />
                          </div>
                        )
                      }
                      {
                        subpost.acf.video_iframe && (
                          <div className="postVideoPlayer">
                            <VideoPlayer iframe={`${subpost.acf.video_iframe}/autostart/false`} />
                          </div>
                        )
                      }
                      {
                        subpost.acf.video && !subpost.acf.video_iframe && (
                          <div className="postVideoPlayer">
                            <VideoPlayer url={subpost.acf.video} autoplay={false} controls={true} muted={false} />
                          </div>
                        )
                      }
                      {
                        asocExpandida ? (
                          <WordpressRawContent raw={subpost.raw} />
                        ) : (
                            <div className="morewrap">
                              <button className="more" to={`${subpost.slug}`}>Ver artículo completo <FontAwesomeIcon icon={['far', 'eye']} /></button>
                            </div>
                          )
                      }

                    </Overall>
                  </StSubpost>
                )
              })
            }
          </>
        )
      }
      {
        single && (
          <div className="share-wrap">
            <Share
              socialConfig={{
                twitterHandle: 'latercera',
                config: {
                  url: `https://interactivo.latercera.com/los-videos-del-estallido-social/${slug}/`,
                  title: `${title} | Los Videos del Estallido Social - Interactivos La Tercera`,
                  hashtags: ['18D']
                }
              }}
            />
          </div>
        )
      }
      {
          single && (
          <StBackButton>
            <Link className="backbutton" to="/">
              Volver
            </Link>
          </StBackButton>
          )
        }
      <Related actualSlug={slug} />
    </StContent>
  </Container>
  )
}

export default Content