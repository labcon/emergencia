
import React, {useState} from "react"
import Title from "./Title"
import { Link } from 'gatsby'
import Image from '../Image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { StBlock } from "./style.css"

const Block = ({ to, videosrc, imgurl, bajada, imgopacity, ...rest}) => {
  const [shouldPlay, updateShouldPlay] = useState(false)
  const [shouldShowVideo, updateShouldShowVideo] = useState(false)
  const [shouldOverlay, updateShouldOverlay] = useState(false)
  const [shouldMute, updateShouldMute] = useState(false)
  const [shouldLoop, updateShouldLoop] = useState(false)

  const handleVideoClick = () => {
    updateShouldShowVideo(true)
    updateShouldPlay(true)
  }

  const handleVideoEnd = () => {
    updateShouldOverlay(true)
    updateShouldMute(true)
    updateShouldLoop(true)
  }

  const handleOverlayClose = () => {
    updateShouldShowVideo(false)
    updateShouldOverlay(false)
  }

  const handleVideoPause = () => {
    updateShouldOverlay(true)
    updateShouldShowVideo(true)
    updateShouldPlay(false)
  }

  const LinkClick = ({children}) => {
    return (
      <Link className="blockLinkClick" to={`/${to}/`}>
        {children}
      </Link>
    )
  }

  return (
    <StBlock>
      <LinkClick>
        <div className="blockSticky">
          <div className="blockWrap">
            <div className="blockText" onClick={handleVideoClick}>
              <div className="blockTextTop">
                <Title {...rest} />
              </div>
              <div className="blockTextBottom">
                <div className="excerpt" dangerouslySetInnerHTML={{ __html: bajada }} />
                <span className="button-more">
                  <FontAwesomeIcon className="icon" icon={['far', 'eye']} />
                    Ver Artículo
                </span>
              </div>
            </div>
            <div className={`${imgopacity ? 'withOpacity' : ''} blockBg`}>
              {imgurl && <Image originalurl={imgurl} alt="" />}

            </div>
          </div>
        </div>
      </LinkClick>
    </StBlock>
    )

}
export default Block