import React, {useEffect, useReducer} from 'react'
import get from 'lodash/get'
import { Waypoint } from 'react-waypoint'
import StoryCard from '@/components/StoryCard'
import HeroPhoto from '@/components/VideoHero/HeroPhoto'
import { StSuperStoryCard } from './style.css'
import { StLink } from '@/components/StoryCard/style.css'
import {Sticky, ContainerPhotos, ContainerTitle} from '@/components/VideoHero/style.css'

const SuperStoryCard = ({ post }) => {
  const imagenes = get(post, 'acf.imagenes_inicio')

  const reducer = (state, action) => {
    switch(action.type) {
      case 'ADD_IMAGE':
        return {
          ...state,
          images: [...state.images, action.image]
        }
      case 'CHANGE_ALL_IMAGES':
        return {
          ...state,
          images: state.images.map(image => {
            return {
              ...image,
              hidden: action.hide
            }
          })
        }
      case 'SHOW_TITLE':
        return {
          ...state,
          showTitle: action.showTitle
        }
      default:
        return state
    }
  }

  const initialState = {
    showTitle: false,
    images: []
  }

  const [state, dispatch] = useReducer(reducer, initialState)

  const addImage = (image) => {
    dispatch({ type: 'ADD_IMAGE', image })
  }

  const showTitle = (showTitle) => {
    dispatch({ type: 'SHOW_TITLE', showTitle })
  }

  const changeAllImages = (hide) => {
    dispatch({ type: 'CHANGE_ALL_IMAGES', hide })
  }

  useEffect(() => {
    imagenes.map((imageurl, index) =>
      addImage({
        url: imageurl,
        id: index,
        hidden: false,
        order: 0
      })
    )
  }, [])

  return (
    <StLink to={`/${post.slug}`}>
    <StSuperStoryCard>
      <Sticky>
        {
          state.images && (
            <ContainerPhotos>
              {
                state.images.map((item, index) => {
                  return (
                    <HeroPhoto image={item.url} key={index} photoid={item.id} hiddenPhoto={item.hidden} />
                  )
                })
              }
            </ContainerPhotos>
          )
        }
        <ContainerTitle>
          <StoryCard post={post}></StoryCard>
        </ContainerTitle>
      </Sticky>
      <Waypoint onEnter={() => changeAllImages(true)}>
      </Waypoint>

    </StSuperStoryCard>
    </StLink>
  )
}

export default SuperStoryCard