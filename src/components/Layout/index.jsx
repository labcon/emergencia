import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"

import Header from "../Header"
import Footer from "../Footer"

import '@fortawesome/fontawesome-svg-core/styles.css'

import GlobalStyle from '../../styles/global.css'

import { library, config } from '@fortawesome/fontawesome-svg-core'
import { faFacebookF, faTwitter, faWhatsapp } from '@fortawesome/free-brands-svg-icons'
import { faChevronDown, faChevronUp, faChevronRight, faChevronLeft, faTimes, faBars, faPlay, faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { faPlayCircle, faEye } from '@fortawesome/free-regular-svg-icons'
import ProgressBar from '../ProgressBar'

config.autoAddCss = false

library.add(faFacebookF, faTwitter, faWhatsapp, faChevronDown, faChevronUp, faChevronRight, faChevronLeft, faTimes, faBars, faPlayCircle, faEye, faPlay, faArrowRight)

const Layout = ({ sponsorLogo, children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <GlobalStyle />
        <ProgressBar height={2} color='tomato' />
        <Header title={data.site.siteMetadata.title} sponsorLogo={sponsorLogo} />
        <div>
          <main>{children}</main>
        </div>
        <Footer />
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
