// import theme from 'styled-theming'
// const backgroundColor = theme('mode', {
//   light: 'white',
//   dark: 'black'
// })

// const textColor = theme('mode', {
//   light: 'black',
//   dark: 'white'
// })

export default {
  background: 'white',
  accent: 'black',
  color: 'black',
  complementary: 'black',
  clearcolor: '#fffff7',
  darkcolor: 'black',
  fontBody: '"Acta Book", Helvetica, Arial, sans-serif',
  fontTitle: '"Acta Display", Helvetica, Arial, sans-serif',
  fontTitleSize: '5.25rem',
  fontSize: '1.2rem',
  lineHeight: '1.42857',
  titleheight: '1',
  normalWidth: '680px',
  maxWidth: '1200px',
  overWidth: '1440px',
  headerHeight: '3rem',
  spacing: '1rem',
  verticalSpacing: '4rem',
  button: {
    backgroundColor: '#fffff7',
    color: 'black',
    borderRadius: '4px',
    padding: '1rem 2rem'
  },
  strong: {
    weight: 800
  }
}