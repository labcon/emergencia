module.exports = {
  siteMetadata: {
    title: `Los videos del estallido social - Interactivos La Tercera`,
    description: `Las movilizaciones y millones de manifestantes en Chile`,
    author: `@latercera`,
    twitterHandle: `@latercera`,
    url: 'https://interactivo.latercera.com/los-videos-del-estallido-social',
    shareimg: `src/images/share.jpg`,
    domain: `https://interactivo.latercera.com`,
    fbappid: `1134891773353659`
  },
  pathPrefix: `/los-videos-del-estallido-social`,
  plugins: [
    {
      resolve: `@labcon/gatsby-plugin-copesa-remote-images`,
      options: {
        nodeType: 'wordpress__POST',
        categoryid: 62,
        extractImageFromPathString: 'content',
        loopPathArray: ['attached_contentimages', 'attached_images_in_acf', 'attached_mainimg', 'attached_audios', 'attached_subtitles_in_acf'],
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-80728886-38`,
      }
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-plugin-facebook-pixel`,
      options: {
        pixelId: '736034173495383',
      },
    },
    {
      resolve: `gatsby-plugin-lodash`,
      options: {
        disabledFeatures: [`shorthands`, `cloning`],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Emergencia`,
        short_name: `emergencia`,
        start_url: `/`,
        background_color: `#000000`,
        theme_color: `#000000`,
        display: `minimal-ui`,
        icon: `src/images/icon-lt.png`
      },
    },
    {
    resolve: 'gatsby-source-wordpress',
      options: {
        baseUrl: 'content.latercera.com',
        protocol: 'http',
        hostingWPCOM: false,
        useACF: true,
        acfOptionPageIds: [],
        verboseOutput: true,
        perPage: 100,
        concurrentRequests: 20,
        includedRoutes: ['**/posts', '**/tags'],
        normalizer: function ({ entities }) {
          return entities;
        }
      }
    },
    `gatsby-plugin-no-sourcemaps`

    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
